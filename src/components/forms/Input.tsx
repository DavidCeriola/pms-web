import React, { FunctionComponent, useEffect, useRef } from 'react';
import { useFormContext } from 'react-hook-form';
import { Input } from '@material-ui/core';

export interface FormTextInputProps {
  id?: string;
  name: string;
  placeholder?: string;
  defaultValue?: string;
  autoFocus?: boolean;
  label?: string;
  onFocus?: ((event: React.FocusEvent<HTMLInputElement>) => void) | undefined;
  onBlur?: ((event: React.FocusEvent<HTMLInputElement>) => void) | undefined;
}

export const CustomInput: FunctionComponent<FormTextInputProps> = ({
  id = '',
  name,
  autoFocus = false,
  ...props
}) => {
  const { register } = useFormContext();
  const inputRef = useRef<HTMLInputElement | null>(null);

  useEffect(() => {
    if (autoFocus && inputRef.current) {
      inputRef.current.focus();
    }
  }, []);

  return (
    <Input
      inputRef={(e: HTMLInputElement) => {
        register(e);
        inputRef.current = e;
      }}
      type='text'
      className='form-control'
      autoComplete='no'
      name={name}
      autoFocus={false}
      {...props}
    />
  );
};
