import React, { FC } from 'react';
import { FormProvider, UseFormMethods } from 'react-hook-form';

interface FormProps {
  provider: UseFormMethods;
  onSubmit: any;
  children: React.ReactNode;
}
export const Form: FC<FormProps> = ({ provider, onSubmit, children }) => {
  const { handleSubmit } = provider;
  return (
    <FormProvider {...provider}>
      <form onSubmit={handleSubmit(onSubmit)}>{children}</form>
    </FormProvider>
  );
};
