import React, { FC } from 'react';
import { Button as BaseButton } from '@material-ui/core';

interface ButtonProps {
  children?: React.ReactNode;
}

export const Button: FC<ButtonProps> = ({ children }) => {
  return <BaseButton>{children}</BaseButton>;
};
