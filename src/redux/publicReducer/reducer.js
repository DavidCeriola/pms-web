const initialState = {
  count: 0,
  loading: false,
};

export default function reducer(state = initialState, { type, payload }) {
  switch (type) {
    case 'COUNT_UP': {
      return {
        ...state,
        count: state.count + 1,
      };
    }

    case 'LOADING': {
      return {
        ...state,
        loading: true,
      };
    }

    case 'UNLOAD': {
      return {
        ...state,
        loading: false,
      };
    }

    default:
      return state;
  }
}
