import React, { useEffect } from 'react';
import './App.scss';
import { useForm } from 'react-hook-form';
import firebase from './firebase/firebase';
import { CustomInput, Form } from './components/';

function App() {
  const methods = useForm();

  const ref = firebase.firestore().collection('users');

  function getUsers() {
    ref.onSnapshot((db) => {
      const allUsers: any = [];
      db.forEach((document) => {
        allUsers.push(document.data());
      });

      console.log(allUsers, 'all');
    });
  }

  useEffect(() => {
    getUsers();
  }, []);
  const onSubmit = (d: any) => window.alert(JSON.stringify(d));
  return (
    <div className='App'>
      <Form onSubmit={onSubmit} provider={methods}>
        <CustomInput name='test' label='Test' />
        <button type='submit'>submit</button>
      </Form>
    </div>
  );
}

export default App;
